﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarTrampa : MonoBehaviour
{
    public GameObject trampa;
    Animator animatorTrampa;
    // Start is called before the first frame update
    void Start()
    {
        animatorTrampa = trampa.GetComponent<Animator>();
    }
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            animatorTrampa.SetTrigger("subirTrampa");
        }
    }


}
