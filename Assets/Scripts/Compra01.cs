﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compra01 : MonoBehaviour
{
    public Text mensaje;
    //public GameObject bloqueador;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            StartCoroutine("Compra1");
        }
    }
    public IEnumerator Compra1()
    {
        mensaje.text = "Manzana comprada";
        yield return new WaitForSeconds(3.0f);
        mensaje.text = "";
        mensaje.text = "Mani comprado -ojo-";
        yield return new WaitForSeconds(3.0f);
        mensaje.text = "";
    }
}
