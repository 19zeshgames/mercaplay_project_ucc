﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tia : MonoBehaviour
{
    public Text mensaje;
    //public GameObject bloqueador;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            StartCoroutine("TiaItems");
        }
    }
    public IEnumerator TiaItems()
    {
        mensaje.text = "Lista de ingredientes obtenida";
        yield return new WaitForSeconds(3.0f);
        mensaje.text = "";
        mensaje.text = "Ve a la galería a comprar los ingredientes";
        yield return new WaitForSeconds(3.0f);
        mensaje.text = "";
    }
}


