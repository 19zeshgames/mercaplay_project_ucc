﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecogerObjeto : MonoBehaviour
{
    public GameController controladorJuego;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    //para colisionar y tomar objetos
    public void OnTriggerEnter(Collider objetoColisiona)
    {

        if (objetoColisiona.tag == "Player")
        {
            controladorJuego.numeroMonedas++;
            controladorJuego.publicarColecciones();
            Debug.Log("Colision>>>");
            Destroy(gameObject, 0.5f);
        }

    }
}
