﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrampaMuere : MonoBehaviour
{
    public Text mensaje;
    public GameObject perroBravo;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Perro")
        {
            StartCoroutine("MuertePerro");
        }
    }
    public IEnumerator MuertePerro()
    {
        mensaje.text = "Perro Aniquilado";
        yield return new WaitForSeconds(1.0f);
        mensaje.text = "";
        Destroy(perroBravo, 0.5f);
    }
}
